#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int arraySum(int arr[], int l){
        int result = 0;

        for(int i = 0; i < l; i++)
            result += arr[i];

        return result;
    }

 int main(){

	ifstream fi("in.txt");
	ofstream fo("2out.txt");

    int makerCount, containersNeeded = 0, maxStorage, filledContainers = 0; fi >> makerCount >> maxStorage; // nuskaito kiek gamintoju ir maksimalu dydi konteinerio
    int arr[maxStorage]; for(int i = 0; i < maxStorage; i++) arr[i] = 0;

    for(int i = 0; i < makerCount; i++){
        int stuffToMove; fi >> stuffToMove; // nuskaito visus gaminius

        for(int j = maxStorage; j > 0; j--){
            int tempContainer = stuffToMove / j;
            arr[maxStorage - j] += tempContainer;
            stuffToMove -= tempContainer * j;
        }

    }

    fo << arraySum(arr, maxStorage) << endl; // kiek konteineriu
    for(int i = 0; i < maxStorage; i++)
        fo << arr[i] << endl;

	fo.close();
	fi.close();

 return 0; }
