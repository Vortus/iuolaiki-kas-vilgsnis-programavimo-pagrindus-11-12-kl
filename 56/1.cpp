#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    int n;
    double all = 0;
    fi >> n;
    double arr[n];

    for(int i = 0; i < n; i++){
      fi >> arr[i];
      all += arr[i];
    }

    double am = all / n, mindelta = abs(am - arr[0]);
    int index = 0;

    for(int i = 1; i < n; i++){
       double j = abs(am - arr[i]);
       if(j < mindelta){ mindelta = j; index = i; }
    }

    cout << index + 1 << " " << arr[index];
    fi.close();

 return 0; }
