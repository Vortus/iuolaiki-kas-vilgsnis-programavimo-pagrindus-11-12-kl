#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    ofstream fo("out.txt");
    int n;
    fi >> n;
    long long arr[n];

    string months[12] = {"sausio" , "vasario", "kovo", "balandzio",
                    "geguzes", "birzelio", "liepos", "rugpjucio",
                    "rugsejio", "spalio", "lapkricio", "gruodzio"};

    for(int i = 0; i < n; i++) // fillup array
        fi >> arr[i];

    for(int i = 0; i < n; i++){ // sortas
        for(int j = i + 1; j < n; j++){
             if(arr[j] < arr[i]){
             long long temp = arr[i];
             arr[i] = arr[j];
             arr[j] = temp;
             }
        }
    }

    bool m = false, w = false;
    for(int i = 0; i < n; i++){ // fillup array
      int p = arr[i] / 10000000000;
      int y = arr[i] / 100000000 % 100;
      int month = arr[i] / 1000000 % 100;
      int day = arr[i] / 10000 % 100;

      if(!m && p % 2 != 0){// first man
        fo << "Vyriausias vyras gime: ";
        fo << "19"; if(y < 10) fo << "0"; fo << y << " m. ";
        fo << months[month - 1] << " " << day << " d.";
        fo << endl;
        m = true;
      }
      if(!w && p % 2 == 0){//first woman
        fo << "Vyriausia moteris gime: ";
        fo << "19"; if(y < 10) fo << "0"; fo << y << " m. ";
        fo << months[month - 1] << " " << day << " d.";
        fo << endl;
        w = true;
      }

    }
    if(!m) fo << "Vyru nera" << endl;
    if(!w) fo << "Moteru nera" << endl;

    fi.close();
    fo.close();

 return 0; }
