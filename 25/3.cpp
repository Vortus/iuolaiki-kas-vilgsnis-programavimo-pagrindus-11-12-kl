#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("3in.txt");
    int n; fi >> n;

    for(int i = 0; i < n; i++){
        double sum = 0;
        for(int d = 0; d < 7; d++){
            int j; fi >> j;
            sum += j;
        }
        cout << i + 1 << " " << fixed << setprecision(3) << sum / 7 << endl;
    }

    fi.close();

 return 0; }
