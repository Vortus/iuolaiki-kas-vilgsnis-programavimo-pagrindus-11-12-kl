#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int x, y; cin >> x >> y;

    for(int i = x; i <= y; i++){
        for(int j = x; j <= y; j++){
            if(i != j && i % j == 0){
                cout << i << " + " << j <<  " = " << i + j << endl;
                cout << i << " - " << j <<  " = " << i - j << endl;
                cout << i << " * " << j <<  " = " << i * j << endl;
                cout << i << " / " << j <<  " = " << i / j << endl;
                cout << "******************" << endl;
            }
        }
    }


 return 0; }
