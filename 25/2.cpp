#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("2in.txt");

    int w, h; fi >> h >> w;
    double allSum = 0;
    for(int i = 0; i < h; i++){
        double sum = 0;
        for(int d = 0; d < w; d++){
            double j; fi >> j;
            sum += j;
        }
        allSum += sum;
        cout << i + 1 << " " << fixed << setprecision(2) << sum << endl;
    }
    fi.close();

    cout << fixed << setprecision(2) << allSum;

 return 0; }
