#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int d, s, r, g;
    cin >> d >> s >> r >> g;
    cout << fixed << setprecision(2)
         << (double)g / (d + s + r);

 return 0; }
