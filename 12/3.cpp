#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int k, t, d, v, n, b;
    cin >> k >> t >> d >> v >> n >> b;

    double allMoney = (k * t) - (k * v * d);
    double driveCost = 320 * b / 100 * n;

    if(allMoney >= driveCost) cout << "Gali";
    else cout << "Negali";

 return 0; }
