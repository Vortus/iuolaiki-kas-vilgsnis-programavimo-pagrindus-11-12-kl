#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

     int t1, k, t;
     cin >> t1 >> k >> t;
     int result = 0;

     while(t - t1 > 0){
        t -= t1;
        t1 += k;
        result++;
     }

     cout << result;

 return 0; }
