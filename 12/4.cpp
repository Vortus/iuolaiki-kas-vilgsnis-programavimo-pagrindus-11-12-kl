#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int n, m; cin >> n >> m;
    double allSeconds = 0;

    for(int i = 0; i < n; i++){
        int j; cin >> j;
        allSeconds += j;
    }

    cout << fixed << setprecision(1)
         << n * m / allSeconds << " m/s ";
    cout << (int)allSeconds;

 return 0; }
