
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

    int n;
	cout << "Iveskite iki kiek norite konvertuoti: "; cin >> n;
	cout << endl;
	cout << "Metrai" << setw(8) << "Coliai" << setw(8) << "Pedos" << setw(8) << "Jardai" << endl;

	for(int i = 1; i <= n; i++){
        cout << i << setw(13) << i * (100 / 2.54) << setw(10) << i * (100 / 2.54) / 12 << setw(10) << i * (100 / 2.54) / 12 / 3 << endl;
	}

 return 0; }
