#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int convertTime(int h, int m){
        return h * 60 + m;
    }

 int main(){

    ifstream fi("in.txt");
    int startH, startM, endH, endM;
    fi >> startH >> startM >> endH >> endM;
    int startTime = convertTime(startH, startM);
    int endTime = convertTime(endH, endM);
    int allTime = endTime - startTime;
    int n; fi >> n;

    for(int i = 0; i < n; i++){
        int a, b, c, d; fi >> a >> b >> c >> d;
        int aa = convertTime(a, b);
        int bb = convertTime(c, d);
        if(endTime > aa){
            if(endTime < bb){ allTime -= endTime - aa; }
            else allTime -= bb - aa;
        }
    }

    cout << allTime;

    fi.close();

 return 0; }
