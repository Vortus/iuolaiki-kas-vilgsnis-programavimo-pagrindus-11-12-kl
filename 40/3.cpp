
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    void varza(int &i, int &u, int &r)
    {
        if (i == 0) i = u / r;
        else if(u == 0) u = i * r;
        else if(r == 0) r = u / i;
    }

 int main()
 {
     int bandymai;
     fstream fr("in.in");
     fr >> bandymai;
     cout << "Bandymu rezultatai" << endl << endl;
     cout << "I " << "U " << "R" << endl;
     for(int j = 0; j < bandymai; j++)
     {
        int i, u, r;
        fr >> i >> u >> r;
        varza(i,u,r);
        cout << i << " " << u << " " << r << endl;
     }

    fr.close();
 return 0; }
