
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    void litrai(int kl, int &k5, int &k2, int &k1){
        k5 = kl / 5;
        k2 = (kl % 5) / 2;
        k1 = (kl % 5 % 2);
    }

 int main(){

     int fermeriuKiekis;
     fstream fr("in.in");
     fr >> fermeriuKiekis;

     for(int i = 0; i < fermeriuKiekis; i++){
        int kl, k5, k2, k1;
        fr >> kl;
        litrai(kl, k5, k2, k1);
        cout << k5 << " " << k2 << " " << k1 << endl;
     }


 return 0; }
