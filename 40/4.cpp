#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int dbd(int a, int b){
        if(b == 0) return a;
        return dbd(b, a % b);
    }

    void suma(int &a1, int &a2, int b1, int b2)
    {
        int bendrasd = a2 != b2 ? a2 * b2 : a2;
        int sum = (a1 * (bendrasd / a2)) + (b1 * (bendrasd / b2));
        a1 = sum;
        a2 = bendrasd;
    }

    void skirtums(int &a1, int &a2, int b1, int b2)
    {
        int bendrasd = a2 != b2 ? a2 * b2 : a2;
        int sum = (a1 * (bendrasd / a2)) - (b1 * (bendrasd / b2));
        a1 = sum;
        a2 = bendrasd;
    }

    void sandaug(int &a1, int &a2, int b1, int b2)
    {
        int dbdd = dbd(a1 * b1, a2 * b2);
        a2 = a2 != b2 ? (a2 * b2) / dbdd : a2;
        a1 = (a1 * b1) / dbdd;
    }


    void dalyba(int &a1, int &a2, int b1, int b2)
    {
        sandaug(a1, a2, b2, b1);
    }

 int main()
 {
     int bandymai;
     fstream fr("in.in");
     fr >> bandymai;

     cout << "T1" << setw(7) << "T2" << setw(9) << "Suma" << setw(13)
          << "Skirtumas" << setw(10) << "Sandauga" << setw(9) << "Dalmuo" << endl;
     for(int j = 0; j < bandymai; j++)
     {
        int a1, a2, b1, b2;
        fr >> a1 >> a2 >> b1 >> b2;
        cout << a1 << "/" << a2 << setw(5) << b1 << "/" << b2;
        //sum
        int x1 = a1, x2 = a2, y1 = b1, y2 = b2;
        suma(x1,x2,y1,y2);
        cout << setw(5) << x1 << "/" << x2;
        //

        //skirt
        x1 = a1; x2 = a2; y1 = b1; y2 = b2;
        skirtums(x1,x2,y1,y2);
        cout << setw(5) << x1 << "/" << x2;
        //

        //daug
        x1 = a1; x2 = a2; y1 = b1; y2 = b2;
        sandaug(x1,x2,y1,y2);
        cout << setw(8) << x1 << "/" << x2;
        //

        //dalyba
        x1 = a1; x2 = a2; y1 = b1; y2 = b2;
        dalyba(x1,x2,y1,y2);
        cout << setw(8) << x1 << "/" << x2;
        //

        cout << endl;
     }

    fr.close();
 return 0; }
