#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void sortAscending(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] > arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

 int main(){

    ifstream fi("4in.txt");
    int n; fi >> n;
    int bArr[n]; for(int i = 0; i < n; i++) fi >> bArr[i];
    fi.close();

    fi.open("4inb.txt");
    int nn; fi >> nn;
    int newBArr[nn]; for(int i = 0; i < nn; i++) fi >> newBArr[i];
    fi.close();

    int arr[n + nn];
    for(int i = 0; i < n; i++)
        arr[i] = bArr[i];

    for(int i = n; i < n + nn; i++)
        arr[i] = newBArr[i - n];

    sortAscending(arr, n + nn);

    for(int i = 0; i < n + nn; i++)
        cout << arr[i] << " ";

 return 0; }
