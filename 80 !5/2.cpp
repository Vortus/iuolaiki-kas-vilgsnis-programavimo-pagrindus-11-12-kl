#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void coutArr(int arr[], int l){
        for(int i = 0; i < l; i++)
            if(arr[i] != -1) cout << arr[i] << " ";
        cout << endl;
    }

    void sortAscending(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] > arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

 int main(){

    ifstream fi("2in.txt");
    int n; fi >> n;
    int RaArr[n]; for(int i = 0; i < n; i++) fi >> RaArr[i];
    fi.close();
    fi.open("2inb.txt");
    int nn; fi >> nn;
    int RiArr[n]; for(int i = 0; i < n; i++){ if(i >= nn) RiArr[i] = -1; else fi >> RiArr[i]; }
    fi.close();

    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            if(RaArr[i] > 99 && RiArr[j] < 100){
                int temp = RaArr[i];
                RaArr[i] = RiArr[j];
                RiArr[j] = temp;
            }
        }
    }

    sortAscending(RaArr, n);
    sortAscending(RiArr, n);

    coutArr(RaArr, n);
    coutArr(RiArr, n);

 return 0; }
