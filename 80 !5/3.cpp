#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void coutArr(int arr[], int l){
        for(int i = 0; i < l; i++)
            cout << arr[i] << " ";
        cout << endl;
    }

    void sortAscending(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] > arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

 int main(){

    ifstream fi("3in.txt");
    int n; fi >> n;
    int firstArr[n]; for(int i = 0; i < n; i++) fi >> firstArr[i];
    fi.close();
    fi.open("3inb.txt");
    int nn; fi >> nn;
    int secondArr[nn]; for(int i = 0; i < nn; i++) fi >> secondArr[i];
    fi.close();

    int arr[n + nn];
    for(int i = 0; i < n; i++)
        arr[i] = firstArr[i];

    for(int i = n; i < n + nn + 3; i++)
        arr[i] = secondArr[i - n];

    sortAscending(arr, n + nn);
    coutArr(arr, n + nn);

 return 0; }

