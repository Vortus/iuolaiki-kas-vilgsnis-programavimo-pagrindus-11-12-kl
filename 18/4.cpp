#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    int n, x, b; fi >> n >> x >> b;

    int mins = 0;
    for(int i = 0; i < n; i++){
        int a, d; fi >> a >> d;
        mins += a + (x - d) * b;
    }

    fi.close();

    cout << mins;

 return 0; }
