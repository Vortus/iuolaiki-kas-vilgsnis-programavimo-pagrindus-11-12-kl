#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    double n, sum = 0;
    fi >> n;

    for(int i = 0; i < n; i++){
        double j; fi >> j;
        sum += j;
    }

    cout << fixed << setprecision(2) << sum;
    fi.close();

 return 0; }
