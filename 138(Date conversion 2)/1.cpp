#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Person{
        string name;
        int nr;
        double costB, costE, costW;
    };

    int n, nn;

    void coutPerson(Person arr[], int i){
        cout << setw(19) << arr[i].name << setw(5) << arr[i].nr << fixed << setprecision(2)
             << setw(9) << arr[i].costB << setw(9) << arr[i].costE << setw(9) << arr[i].costW << endl;
    }

    void calculatePaid(Person people[], Person paidPeople[]){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < nn; j++){
                if(people[i].name == paidPeople[j].name && people[i].nr == paidPeople[j].nr){
                    people[i].costB += paidPeople[j].costB;
                    people[i].costE += paidPeople[j].costE;
                    people[i].costW += paidPeople[j].costW;
                }
            }
        }
    }

    void coutLine(int n){
        for(int i = 0; i < n; i++)
            cout << "-";
        cout << endl;
    }

    bool inDept(Person arr[], int i){
        if(arr[i].costB < 0 || arr[i].costE < 0 || arr[i].costW < 0) return true;
        return false;
    }

 int main(){

    /////////////////////////////nuskaitomi failai
    ifstream fi("1in.txt");
    fi >> n; fi.ignore();
    Person people[n];

    for(int i = 0; i < n; i++){
        char name[20]; fi.get(name, sizeof name);
        double a, b, c, d; fi >> a >> b >> c >> d; fi.ignore();
        people[i] = { name, a, b, c, d };
    }

    fi.close();
    fi.open("1inb.txt");
    fi >> nn; fi.ignore();

    Person paidPeople[nn];
    for(int i = 0; i < nn; i++){
        char name[20]; fi.get(name, sizeof name);
        double a, b, c, d; fi >> a >> b >> c >> d; fi.ignore();
        paidPeople[i] = { name, a, b, c, d };
    }
    fi.close();
    /////////////////////////////

    calculatePaid(people, paidPeople);

    //visi
    cout << left << setw(19) << "Pavarde, vardas" << setw(5) << "Nr." << fixed << setprecision(2)
         << setw(9) << "Sildymas" << setw(9) << "Elektra" << setw(9) << "Vanduo" << endl;
    coutLine(51);
    for(int i = 0; i < n; i++)
        coutPerson(people, i);
    coutLine(51);

    //skoolingi
    cout << left << setw(19) << "Pavarde, vardas" << setw(5) << "Nr." << fixed << setprecision(2)
         << setw(9) << "Sildymas" << setw(9) << "Elektra" << setw(9) << "Vanduo" << endl;
    coutLine(51);
    for(int i = 0; i < n; i++)
        if(inDept(people, i)) coutPerson(people, i);
    coutLine(51);

 return 0; }
