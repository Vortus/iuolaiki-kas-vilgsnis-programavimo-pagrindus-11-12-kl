#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Person {
        string name;
        int y, m, d, time;
        double cost;
    };

    int n;
    const int months[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    bool leapYear(int yr){
        if ((yr % 4 == 0) && !(yr % 100 == 0)|| (yr % 400 == 0)) return true;
        return false;
    }

    void calculateTravel(int &y, int &m, int &d, int time){
        int factor = months[m - 1]; if(m == 2 && leapYear(y)) factor++;
        d += time;
        while(d > factor){
            d -= factor;
            if(m + 1 > 12){ y++; m = 1; }
            else m++;
            factor = months[m - 1]; if(m == 2 && leapYear(y)) factor++;
        }
        d--;
    }

    void sortDescending(Person arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].cost < arr[j].cost){
                    Person temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

 int main(){

    ////////////////////////
    ifstream fi("3in.txt");
    fi >> n; fi.ignore();
    Person people[n];

    for(int i = 0; i < n; i++){
        char name[20]; fi.get(name, sizeof name);
        double a, b, c, d, e; fi >> a >> b >> c >> d >> e;
        fi.ignore();
        people[i] = { name, a, b, c, d, d * e };
    }
    fi.close();
    ////////////////////////

    sortDescending(people);
    for(int i = 0; i < n; i++){
    int a = people[i].y, b = people[i].m, c = people[i].d,
        d = people[i].time; double e = people[i].cost;
    calculateTravel(a, b, c, d);

    cout << people[i].name << a << " " << b << " " << c << " " << fixed << setprecision(2) << people[i].cost << endl;

    }
 return 0; }
