#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Item{
        string name;
        double y, m, d, used;
    };

    int startDate, endDate, n;

    double convertTime(int y, int m, int d) {
        if (m < 3)
            y--, m += 12;
        return 365 * y + y / 4 - y / 100 + y / 400 + (153 * m - 457) / 5 + d - 306;
    }

    void sortAlpha(Item arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].name > arr[j].name){
                    Item temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    void coutItem(Item arr[], int i){

        cout << setw(19) << arr[i].name << setw(7) << arr[i].y << setw(7);
        if(arr[i].m < 10) cout << "0"; cout << arr[i].m << setw(6);
        if(arr[i].d < 10) cout << "0"; cout << arr[i].d << setw(7) << arr[i].used << endl;
    }


 int main(){

    int a, b, c, d;
    cout << "Iveskite pradzios laik.: "; cin >> a >> b >> c;
    startDate = convertTime(a, b, c);
    cout << "Iveskite pabaigos laik.: "; cin >> a >> b >> c; cout << endl;
    endDate = convertTime(a, b, c);

    ifstream fi("2in.txt");
    fi >> n; fi.ignore();
    Item items[n];

    for(int i = 0; i < n; i++){
        char name[20]; fi.get(name, sizeof name);
        fi >> a >> b >> c >> d; fi.ignore();
        items[i] = { name, a, b, c, d };
    }

    fi.close();

    sortAlpha(items);
    double average = 0, nn = 0;
    for(int i = 0; i < n; i++){
        int time = convertTime(items[i].y, items[i].m, items[i].d);
        if(time <= endDate && time >= startDate){
                average += items[i].used;
                coutItem(items, i);
                nn++;
        }
    }

    cout << "Vidurkis: " << fixed << setprecision(2) << average / nn;
 return 0; }
