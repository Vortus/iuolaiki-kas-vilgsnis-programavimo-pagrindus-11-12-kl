#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Person{
        string name, city;
        double mins;
    };

    int n;

    void fixArray(Person arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].name == arr[j].name){
                    arr[i].mins += arr[j].mins;
                    arr[j].mins = -1;
                }
            }
        }
    }

    void sortAlphabeticly(Person arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].name > arr[j].name){
                    Person temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    void calculateCost(Person arr[], string city, double cost){
        for(int i = 0; i < n; i++)
            if(arr[i].city == city) arr[i].mins *= cost;
    }

 int main(){

    ifstream fi("4in.txt");
    fi >> n; fi.ignore();
    Person people[n];

    for(int i = 0; i < n; i++){
        char name[20]; fi.get(name, sizeof name);
        long long number; fi >> number;
        string city; fi >> city;
        int mins; fi >> mins; fi.ignore();
        people[i] = { name, city, mins };
    }

    fi.close();
    fi.open("4inb.txt");
    int nn; fi >> nn;

    for(int i = 0; i < nn; i++){
        string city; fi >> city;
        double cost; fi >> cost;
        calculateCost(people, city, cost);
    }

    fi.close();

    fixArray(people);
    sortAlphabeticly(people);


    double income = 0;
    for(int i = 0; i < n; i++)
        if(people[i].mins > -1){
            income += people[i].mins;
            cout << people[i].name << people[i].city << " " << fixed << setprecision(2) << people[i].mins << endl; }

    cout << "Pajamos: " << fixed << setprecision(2) << income;

 return 0; }
