#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    void sortArray(int arr[], int size){
        for(int i = 0; i < size; i++){
            for(int j = i + 1; j < size; j++){
                int temp = arr[i];
                if(arr[i] < arr[j]){ arr[i] = arr[j]; arr[j] = temp; }
            }
        }
    }

 int main(){

    ifstream fi("in.txt");
    int allmoney, cases, bought = 0;
    fi >> allmoney >> cases;

    for(int i = 0; i < cases; i++){

        int casesize, startmoney = allmoney;
        fi >> casesize;
        int costs[casesize];

        for(int j = 0; j < casesize; j++)
            fi >> costs[j];

        sortArray(costs, casesize);

        for(int j = 0; j < casesize; j++)
            if(startmoney - costs[j] >= 0){ allmoney -= costs[j]; bought++; break; }
    }

    cout << bought << " " << allmoney;
    fi.close();

 return 0; }
