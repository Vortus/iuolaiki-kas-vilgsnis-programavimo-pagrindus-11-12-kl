#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>

using namespace std;

 int main(){

     int xMax = 0, yMax = 0, xMin = 99999, yMin = 99999, n = 0;
     fstream fr("in.in");
     fr >> n;

     for(int i = 0; i < n; i++){
        int x1, y1, x2, y2;
        fr >> x1 >> y1 >> x2 >> y2;
        if(x1 > xMax) xMax = x1;
        if(y1 > yMax) yMax = y1;
        if(x2 < xMin) xMin = x2;
        if(y2 < yMin) yMin = y2;
     }

     fr.close();

     int s = abs(yMin - yMax) * abs(xMin - xMax);
     ofstream fp("out.out");
     fp << s;
     fp.close();

 return 0; }

