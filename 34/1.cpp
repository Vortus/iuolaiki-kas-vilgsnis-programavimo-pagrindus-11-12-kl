
#include <io.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>

using namespace std;

 int main(){

     int sienosP, sienosA, plakatuKiekis, sienosPlotas;
     fstream fr("in.in");
     fr >> sienosP;
     fr >> sienosA;
     fr >> plakatuKiekis;
     sienosPlotas = sienosP * sienosA;

     for(int i = 0; i < plakatuKiekis; i++){
        int x1, x2, y1, y2;
        fr >> x1 >> y1 >> x2 >> y2;
        sienosPlotas -= abs(y2 - y1) * abs(x2 - x1);
     }
     fr.close();
     ofstream fp("out.out");
     fp << sienosPlotas;

 return 0; }
