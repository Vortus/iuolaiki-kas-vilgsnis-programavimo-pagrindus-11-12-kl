
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

double plotas(int ax, int ay, int bx, int by, int cx, int cy);
double atstumasTarp(int x1, int y1, int x2, int y2);

 int main(){

     double ax, ay, dx, dy, bx, by, cx, cy;
     fstream fr("in.in");
        fr >> ax >> ay;
        fr >> bx >> by;
        fr >> cx >> cy;
        fr >> dx >> dy;

     fr.close();

    double plotasAbc = plotas(ax, ay, bx, by, cx, cy);
    double plotasAbd = plotas(ax, ay, bx, by, dx, dy);
    double plotasBcd = plotas(bx, by, cx, cy, dx, dy);
    double plotasAcd = plotas(ax, ay, cx, cy, dx, dy);

    double sump = plotasAbd + plotasBcd + plotasAcd;
    double abss = abs(plotasAbc - sump);

    if((abss < 0.001)){
        cout << "Trikampis yra viduje";
        return 0;
    }else{
        cout << "Trikampis nera viduje";
        return 0;
    }


 return 0; }

 double plotas(int ax, int ay, int bx, int by, int cx, int cy){

    double a = atstumasTarp(ax, ay, bx, by);
    double b = atstumasTarp(bx, by, cx, cy);
    double c = atstumasTarp(ax, ay, cx, cy);
    double p = (a + b + c) / 2;// pusperimetris

    return sqrt(p * (p - a) * (p - b) * (p - c));;
 }

 double atstumasTarp(int x1, int y1, int x2, int y2){
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
 }


