#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct OwnedAnimal {
        string name;
        int ownedCount;
    };

    int n;

    void sortAlphabeticly(OwnedAnimal arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].name < arr[j].name){
                    OwnedAnimal temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    void modifyArrray(OwnedAnimal arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].name == arr[j].name){
                    arr[i].ownedCount += arr[j].ownedCount;
                    arr[j].ownedCount = -1;
                }
            }
        }
    }

    void sortDescending(OwnedAnimal arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].ownedCount < arr[j].ownedCount){
                    OwnedAnimal temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

 int main(){

    ifstream fi("3in.txt");
    fi >> n; fi.ignore();
    OwnedAnimal animals[n];

    for(int i = 0; i < n; i++){
        char name[15]; fi.get(name, sizeof name);
        int j; fi >> j; fi.ignore(80, '\n');
        animals[i] = { name, j };
    }

    fi.close();
    sortAlphabeticly(animals);
    modifyArrray(animals);
    sortDescending(animals);

    for(int i = 0; i < n; i++)
        if(animals[i].ownedCount > -1) cout << animals[i].name << animals[i].ownedCount << endl;

 return 0; }
