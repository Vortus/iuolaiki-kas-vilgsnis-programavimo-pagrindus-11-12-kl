#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Candidate {
        string name, last;
        int votes;
    };

    int n;

   void sortDescending(Candidate arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].votes < arr[j].votes){
                    Candidate temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
   }

   void calculateWithDuplicates(Candidate arr[]){
        for(int i = 0; i < n; i++){
            arr[i].votes++;
            for(int j = i + 1; j < n; j++){
                if(arr[i].name == arr[j].name && arr[i].last == arr[j].last){
                    arr[i].votes++;
                    arr[j].votes = -1;
                }
            }
        }
    }

 int main(){

    //reading file
    ifstream fi("4in.txt");
    fi >> n; fi.ignore();
    Candidate candidates[n];

    for(int i = 0; i < n; i++){
        char name[15], last[15];
        fi.get(name, sizeof name); fi.get(last, sizeof last);
        fi.ignore();
        candidates[i] = { name, last, 0 };
    }
    fi.close();
    //

    calculateWithDuplicates(candidates);
    sortDescending(candidates);

    for(int i = 0; i < n; i++)
        if(candidates[i].votes > 0)
            cout << candidates[i].name << candidates[i].last << " " << candidates[i].votes << endl;

 return 0; }
