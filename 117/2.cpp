
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    struct Ice{
        string name;
        double cost;
    };

    int n; // ledu sk

    void sortAlphabet(Ice arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].name < arr[j].name){
                Ice temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                }
            }
        }
    }

    void modifyIceArr(Ice arr[]){
        for(int i = n - 1; i > 1; i--){
            for(int j = i - 1; j > 1; j--){
                if(arr[i].name != arr[j].name) break;
                else {
                    arr[i].cost += arr[j].cost;
                    arr[j].cost = -1;
                }
            }
        }
    }

    void sortIce(Ice arr[]){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].cost < arr[j].cost){
                    Ice temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

 int main(){

	ifstream fi("2in.txt");
    fi >> n; fi.ignore();
    Ice iceArr[n];

    for(int i = 0; i < n; i++){
        char name[20]; fi.get(name, sizeof name);
        double a, b; fi >> a >> b; fi.ignore();
        iceArr[i] = { name, a * b };
    }

    sortAlphabet(iceArr);
    modifyIceArr(iceArr);
    sortIce(iceArr);

    for(int i = 0; i < n; i++)
         if(iceArr[i].cost >= 0) cout << iceArr[i].name << iceArr[i].cost << endl;

	fi.close();

 return 0; }
