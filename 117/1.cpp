
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int n; //kiekis ciuvu

    int count(string name, string names[]){
        int result = 0;
        for(int i = 0; i < n; i++)
            if(names[i] == name) result++;
        return result;
    }

    void sortTimes(string names[]){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                int a = count(names[i], names), b = count(names[j], names);
                if(a < b){
                    string temp = names[i];
                    names[i] = names[j];
                    names[j] = temp;
                }
            }
        }
    }

    void sortAlphabet(string names[]){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                if(names[i] <  names[j]){
                    string temp = names[i];
                    names[i] = names[j];
                    names[j] = temp;
                }
            }
        }
    }

    void coutNames(string names[]){
        for(int i = 0; i < n; i++){
            cout << names[i] << " " << count(names[i], names) << endl;
            for(int j = i + 1; j < n; j++){
                if(names[j] != names[i])break;
                i++;
            }
        }
    }


 int main(){

	ifstream fi("1in.txt");
    fi >> n; fi.ignore();
    string names[n];

    for(int i = 0; i < n; i++){
        char last[20]; fi.get(last, sizeof last);
        char name[20]; fi.get(name, sizeof name);
        fi.ignore();
        names[i] = name;
    }

    sortTimes(names);
    sortAlphabet(names);
    coutNames(names);

	fi.close();

 return 0; }
