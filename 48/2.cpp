#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    int n, vcount = 0, mcount = 0, vkom = 0, mkom = 0;
    double vidv = 0, vidm = 0;
    fi >> n;

    double h;
    for(int i = 1; i <= n; i++){
        fi >> h;
        if(h <= 0){ if (abs(h) >= 175) vkom++; vcount++; vidv += abs(h); }
        else { if(h >= 175) mkom++; mcount++; vidm += h; }
    }

    cout << fixed << setprecision(1) << (vidv + vidm) / n << endl;
    cout << fixed << setprecision(2) << vidm / mcount << endl;
    cout << fixed << setprecision(2) << vidv / vcount << endl;

    cout << "Merginu komandos sudaryti ";
    if (mkom >= 7) cout << "galima"; else cout << "negalima"; cout << endl;

    cout << "Vaikinu komandos sudaryti ";
    if (vkom >= 7) cout << "galima"; else cout << "negalima"; cout << endl;

    fi.close();

 return 0; }
