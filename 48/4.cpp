#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    ofstream fo("out.txt");
    int h, w;
    fi >> h >> w;

    int arr[w * h];

    //fillup array
    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++){
           fi >> arr[j + i * w];
        }
    }
    //

    for(int i = 0; i < h; i++)
    {
        for(int j = 0; j < w; j++)
        {
            int sum = arr[j + i * w];
            int d = j == 0 || j == w - 1 ? 2 : 3;
            if(j - 1 >= 0){ sum += arr[(j - 1) + i * w]; }
            if(j + 1 < w){ sum += arr[(j + 1) + i * w]; }
            fo << sum / d << " ";
        }
        fo << endl;
    }

    fi.close();
    fo.close();

 return 0; }
