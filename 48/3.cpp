#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    int n, all = 0;
    fi >> n;

    for(int i = 0; i < n; i++){
        int j;
        fi >> j;
        all += j;
    }

    cout << all << endl;
    cout << all / n << endl;
    cout << fixed << setprecision(1) << all / n / 10.0;

    fi.close();

 return 0; }
