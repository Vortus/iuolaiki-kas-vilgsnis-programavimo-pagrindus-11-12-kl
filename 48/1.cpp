#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in.txt");
    double n;
    int allleft = 0, allright = 0;
    fi >> n;

    for(int i = 1; i <= n; i++){
       int numeric, amount;
       fi >> numeric >> amount;
       if(numeric % 2 == 0) allright += amount;
       else allleft += amount;
    }

    fi.close();

    cout << allleft + allright << endl;
    cout << allleft << endl;
    cout << allright << endl;
    cout << allleft / (n / 2) << endl;
    cout << allright / (n / 2) << endl;

 return 0; }
