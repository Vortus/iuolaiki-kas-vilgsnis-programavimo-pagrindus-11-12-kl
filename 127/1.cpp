#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Album {
        string name;
        int year, hoursListened,
        minutesListened, timesListened;
    };

    int numberOfAlbums;
    Album *albums;

    void albumsLength(Album arr[], int l, int &h, int &m){
        for(int i = 0; i < l; i++){
            h += arr[i].hoursListened;
            m += arr[i].minutesListened;
        }
        h += m / 60; m %= 60;
    }

    double allAverage(Album arr[], int l){
        double result = 0;
        for(int i = 0; i < l; i++)
            result += arr[i].timesListened;
        return result / l;
    }

    void sortAlbums(Album arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i].timesListened >= arr[j].timesListened){
                    Album temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    void readFile(){

    ifstream fi("1in.txt");
        fi >> numberOfAlbums;
        fi.ignore(80, '\n');// nuskaite pirma eilute ir reikia praignorint kadangi fi.get vel skaitys is naujo visa faila.
        albums = new Album[numberOfAlbums];

        for(int i = 0; i < numberOfAlbums; i++){
            char chars[20]; fi.get(chars, sizeof chars); string s = chars; // nuskaito pavadinima
            int a, b, c, d; fi >> a >> b >> c >> d; fi.ignore(); // nuskaito skaicius ir perkelia i kita eilute su fi.ignore() nes paskutinis simbolis yra \n
            albums[i] = { s, a, b, c, d }; // sudeda reiksmes i albumu masyva
        }

    fi.close();

    }

    void coutAlbums(){
        for(int i = 0; i < numberOfAlbums; i++)
            cout << albums[i].name << albums[i].year << endl;
    }

 int main(){

    readFile();
    int allH = 0, allM = 0;
    albumsLength(albums, numberOfAlbums, allH, allM);
    cout << allH << " " << allM << endl;
    cout << fixed << setprecision(2) << allAverage(albums, numberOfAlbums)<< endl;
    sortAlbums(albums, numberOfAlbums);
    coutAlbums();

 return 0; }
