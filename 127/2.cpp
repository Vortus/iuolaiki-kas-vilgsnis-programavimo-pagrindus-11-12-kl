#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>

using namespace std;

    struct Pupil { // struktas mokinio
        string name;
        double grade = 0; // prilygint nuliui nes man kazkodel is lempos vienam mokiniui neleido sudet skaiciu
    };

    //globalus kintamieji
    int pupilCount;
    Pupil *pupils;

    //mokiniu surusiavimas didejancia tvarka priklausant nuo pazymio
    void sortPupilsDescending(){
        for(int i = 0; i < pupilCount; i++){
            for(int j = i + 1; j < pupilCount; j++){
                if(pupils[i].grade < pupils[j].grade){
                    Pupil temp = pupils[i];
                    pupils[i] = pupils[j];
                    pupils[j] = temp;
                }
            }
        }
    }

    //funkcija ieskanti kiek reiksmiu yra atitinkanciu salyga "pazymis >= n"
    int moreThan(int n){
        int result = 0;
        for(int i = 0; i < pupilCount; i++)
            if(pupils[i].grade >= n) result++;
        return result;
    }

    //vidutinis
    int average(){
        double sum = 0;
        for(int i = 0; i < pupilCount; i++)
            sum += pupils[i].grade;
        return round(sum / pupilCount);
    }

    //ispausdinimas mokiniu
    void coutPupils(int start, int l){
        for(int i = start; i < l; i++)
            cout << pupils[i].name << pupils[i].grade << endl;
    }

    void readFile(){ // nuskaito failas
        ifstream fi("2in.txt");
            int testCount; fi >> pupilCount >> testCount;
            double testFactors[testCount]; for(int i = 0; i < testCount; i++) fi >> testFactors[i]; fi.ignore();
            pupils = new Pupil[pupilCount];
            for(int i = 0; i < pupilCount; i++){
                char chars[15]; fi.get(chars, sizeof chars);
                pupils[i].name = chars;
                for(int j = 0; j < testCount; j++){
                    int a; fi >> a;
                    pupils[i].grade += a * testFactors[j];
                }
                pupils[i].grade = round(pupils[i].grade);// suapvalinimas
                fi.ignore();
            }

        fi.close();
    }

    int biggestEndIndex(){ // didziausiu pazymio pabaigos indeksas cia kai surusiuoji suranda geriausiu mokiniu pabaigos indeksa
        int index = 0;
        for(int i = 0; i < pupilCount - 1; i++)
            if(pupils[i].grade == pupils[i + 1].grade) index++;
            else break;
        return index;
    }

    int smallestStartIndex(){ // maziausias pazymio pradsios indeksas cia kai surusiuoji suranda blogiausiu mokiniu pradzios indeksa
        int index = pupilCount - 1;
        for(int i = pupilCount - 1; i > 1 ; i--)
            if(pupils[i].grade == pupils[i - 1].grade) index--;
            else break;
        return index;
    }

 int main(){

    readFile();
    coutPupils(0, pupilCount);
    cout << average() << endl;
    sortPupilsDescending();
    coutPupils(0, biggestEndIndex() + 1); // plius vienas nes ten yra < o ne <=
    coutPupils(smallestStartIndex(), pupilCount);
    cout << moreThan(4);

 return 0; }
