#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Payer{
        string name;
        double moneySpent;
        int bought;
    };

    int n, np;

    void coutPayer(Payer arr[], int i, bool money){
         cout << arr[i].name;
         if(money) cout << fixed << setprecision(2) << arr[i].moneySpent << endl;
         else  cout << arr[i].bought << endl;
    }

    double arrSum(Payer arr[]){
        double result = 0;
        for(int i = 0; i < n; i++)
            result += arr[i].moneySpent;
        return result;
    }

    void showBiggestBuyer(Payer arr[], bool f){
        int index = 0; double maximum = arr[0].moneySpent;
        for(int i = 1; i < n; i++){
            if(f){ if(arr[i].moneySpent > maximum){ maximum = arr[i].moneySpent; index = i; } }
            else {  if(arr[i].moneySpent < maximum){ maximum = arr[i].moneySpent; index = i; } }
        }
        if(f) coutPayer(arr, index, false);
        else  coutPayer(arr, index, true);
    }

 int main(){

    ifstream fi("3in.txt");
    fi >> n >> np;
    Payer payers[n]; double costs[np];
    for(int i = 0; i < np; i++) fi >> costs[i]; fi.ignore();

    for(int i = 0; i < n; i++){
        char chars[15]; fi.get(chars, sizeof chars); int c = 0; double totalM = 0;
        for(int j = 0; j < np; j++){//
            int a; fi >> a; c += a;
            totalM += a * costs[j];
        }

        payers[i] = { chars, totalM, c };
        fi.ignore();
    }

    fi.close();

    for(int i = 0; i < n; i++)
        coutPayer(payers, i, true);

    cout << arrSum(payers) << endl;
    showBiggestBuyer(payers, true);
    showBiggestBuyer(payers, false);


 return 0; }
