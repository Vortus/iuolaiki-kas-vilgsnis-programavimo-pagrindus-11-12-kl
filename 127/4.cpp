#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Child{
        string name;
        int money; //centais
    };

    int n;

    void convertMoney(int &f, int &nf){
        int temp = f;
        f = temp / 100;
        nf = temp % 100;
    }

    int sumArr(Child arr[]){
        int result = 0;
        for(int i = 0; i < n; i++)
            result += arr[i].money;
        return result;
    }

    int biggestAtIndex(Child arr[]){
        int result = 0, maximum = arr[0].money;
        for(int i = 1; i < n; i++)
            if(arr[i].money > maximum) { maximum = arr[i].money; result = i; }
        return result;
    }

    void coutChildren(Child arr[], int i){
        int a = arr[i].money, b = 0;
        convertMoney(a, b);
        cout << arr[i].name << a << " " << b << endl;
    }

 int main(){

    ifstream fi("4in.txt");
    fi >> n; fi.ignore();
    Child children[n];

    for(int i = 0; i < n; i++){
        char name[15]; fi.get(name, sizeof name);
        double a, b, c, sum = 0; fi >> a >> b >> c;
        for(int j = 0; j < c; j++){
            int d; double e; fi >> d >> e;
            sum += d * (e * 100);
        }
        fi.ignore();
        children[i] = { name, (a * 100 + b) - sum };
    }

    fi.close();

    //visi vaikai
    for(int i = 0; i < n; i++){
        coutChildren(children, i);
    }
    //

    //pinigu suma
    int a = sumArr(children), b = 0;
    convertMoney(a, b);
    cout << a << " " << b << endl;
    //

    //dud vk
    coutChildren(children, biggestAtIndex(children));
    //

 return 0; }
