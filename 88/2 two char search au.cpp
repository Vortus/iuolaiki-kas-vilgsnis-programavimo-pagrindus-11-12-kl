#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int getCount(string line){
        int result = 0;
        for(int i = 0; i < line.length() - 1; i++)
            if(line[i] == 'a' && line[i + 1] == 'u')
                 result++;
        return result;
    }

 int main(){

    ifstream fi("3in.txt");
    string line; int c = 0;
    while(!fi.eof()){
        getline(fi, line);
        c += getCount(line);
    }
    fi.close();

    cout << c;

 return 0; }
