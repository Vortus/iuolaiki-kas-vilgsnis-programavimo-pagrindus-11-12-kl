#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    int checkCount(int a, string s){
        int result = 0;
        for(int i = 0; i < s.length(); i++){
           int c = s[i] - '0';
            if(a == c) result++;
        }
        return result;
    }

 int main(){
    int arr[10]; for(int i = 0; i < 10; i++) arr[i] = 0;

    ifstream fi("1in.txt");
    while(!fi.eof()){
        string line; getline(fi, line);
          for(int i = 0; i < 10; i++){
            arr[i] += checkCount(i, line);
          }
    }
    fi.close();

    for(int i = 0; i < 10; i++)
        cout << i << " " << arr[i] << endl;

 return 0; }
