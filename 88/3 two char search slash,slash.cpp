#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int getCount(string line){
        int result = 0;
        for(int i = 0; i < line.length() - 1; i++)
            if(line[i - 1] != '/' && line[i] == '/' && line[i + 1] == '/' )
                if(i - 1 > -1) result++;
        return result;
    }

 int main(){

    ifstream fi("2in.txt");
    string line; int c = 0;
    while(!fi.eof()){
        getline(fi, line);
        c += getCount(line);
    }
    fi.close();

    cout << c;

 return 0; }
