#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int letters = 0, numbers = 0, spaces = 0, other = 0;
	ifstream fi("4in.txt");

    while(!fi.eof()){
        char c = fi.get();
        int cn = (int)c;
        if((cn >= 65 && cn <= 122) || (cn >= 192 && cn <= 254)) letters++; // visos raides su lt ir netik lt
        else if(cn >= 48 && cn <= 57) numbers++; // skaicius
        else if(c == ' ') spaces++; // tarpai
        else if(cn >= 33 && cn <= 122) other++; // kiti
    }

    cout << "Raidziu yra " << letters << endl;
    cout << "Skaitmenu yra " << numbers << endl;
    cout << "Tarpo simboliu yra " << spaces << endl;
    cout << "Kitokiu simboliu yra " << other;

	fi.close();

 return 0; }
