#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
using namespace std;

    string codedLetter(char letter, char arr[], string arrCoded[], int l){
        for(int i = 0; i < l; i++)
            if(letter == arr[i]) return arrCoded[i];
        return "";
    }

 int main(){
    //failo nuskaitymas
    ifstream fi("5in.txt");
        int n; fi >> n;
        char letters[n]; string codedLetters[n];
        for(int i = 0; i < n; i++) fi >> letters[i] >> codedLetters[i];
    fi.close();

    ifstream fi1("5inw.txt");
        string word; getline(fi1, word);
        char *wordChars = new char[word.length() + 1];
        strcpy(wordChars, word.c_str());
    fi1.close();
    //

    for(int i = 0; i < word.length(); i++){
        if(i != 0 && wordChars[i - 1] != ' ') cout << " ";
        if(wordChars[i] == ' ') cout << endl;
        cout << codedLetter(wordChars[i], letters, codedLetters, n);
    }

 return 0; }
