#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
using namespace std;

    bool contains(char arr[], char c, int l){
        for(int i = 0; i < l; i++)
            if(arr[i] == c) return true;
        return false;
    }

 int main(){

    ifstream fi("in.txt");
    int n; fi >> n;
    fi.ignore(80, '\n');

    for(int i = 0; i < n; i++){
        string line; getline(fi, line);
        char cArr[line.length() + 1]; strcpy(cArr, line.c_str());
        if(contains(cArr, ' ', line.length())) cout << line << endl;
    }

    fi.close();

 return 0; }
