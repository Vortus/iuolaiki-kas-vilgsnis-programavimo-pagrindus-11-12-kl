#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

 int largestAtIndexY(double arr[], int xOff, int n){
    int index = 0;
    double maximum = arr[xOff];
    for(int i = 0; i < n; i++){
        double j = arr[xOff + i * 5];
        if(j > maximum){ maximum = j; index = i; }
    }
    return maximum > 0 ? index : -1;
 }

 int main(){

    ifstream fi("in.txt");
    int n;
    fi >> n;
    double arr[n * 5];

    for(int i = 0; i < n * 5; i++)
    fi >> arr[i];

    int bestday = largestAtIndexY(arr, 2, n);
    int mostb = largestAtIndexY(arr, 3, n);
    int mostr = largestAtIndexY(arr, 4, n);

    if(bestday == -1) cout << "Derlingiausia diena: nera" << endl;
    else cout << "Derlingiausia diena: " << arr[0 + bestday * 5] << " " << arr[1 + bestday * 5] << endl;

    if(mostb == -1) cout << "Derlingiausia baravyku diena: nera" << endl;
    else cout << "DDerlingiausia baravyku diena: " << arr[0 + mostb * 5] << " " << arr[1 + mostb * 5] << endl;

    if(mostr == -1) cout << "Derlingiausia raudonvirsiu diena: nera" << endl;
    else cout << "Derlingiausia raudonvirsiu diena: " << arr[0 + mostr * 5] << " " << arr[1 + mostr * 5];

    fi.close();

 return 0; }
