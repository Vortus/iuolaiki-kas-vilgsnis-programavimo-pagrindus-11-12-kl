#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    double distance(double x1, double y1, double x2, double y2){
        return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

 int main(){

    ifstream fi("in.txt");
    int d, n;
    fi >> d >> n;
    double arr[d * 2];

    for(int i = 0; i < d * 2; i++)
        fi >> arr[i];

    double fulldistance = 0, minsp = 99999999999, maxsp = 0, fullspeed = 0;

    for(int i = 0; i < d * 2 - 2; i+= 2){
        double x1 = arr[i], y1 = arr[i + 1], x2 = arr[i + 2], y2 = arr[i + 3];
        double dist = distance(x1, y1, x2, y2);
        double speed = dist / n;
        if(speed < minsp){ minsp = speed; }
        if(speed > maxsp){ maxsp = speed; }
        fulldistance += dist;
        fullspeed += speed;
    }

    cout << "Maziausias greitis: " << fixed << setprecision(2) << minsp << " mm/s"  << endl;
    cout << "Vidutinis greitis: " << fixed << setprecision(2) << fullspeed / (d - 1) << " mm/s" << endl;
    cout << "Didziausias greitis: " << fixed << setprecision(2) << maxsp << " mm/s"  << endl;
    cout << "Nukeliautas atstumas: " << fixed << setprecision(2) << fulldistance << " mm" << endl;

    fi.close();

 return 0; }
