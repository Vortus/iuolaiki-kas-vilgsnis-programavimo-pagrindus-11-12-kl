#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

    int shine(int prev, int next, int d){
        if(prev == 1 && next == 1) return 0;
        else if((prev == 1 && next == 0) || (prev == 0 && next == 1)) return 1;
        else return d % 2 == 0 ? 1 : 0;
    }

    int arrSum(int arr[], int l){
        int r = 0;
        for(int i = 0; i < l; i++)
            r += arr[i];
        return r;
    }

 int main(){

    ifstream fi("in.txt");
    int n, d, maxindex = 1, minindex = 1, mins = 9999999, maxs = 0;
    fi >> n >> d;
    int arr[n];

    for(int i = 0; i < n; i++)
        fi >> arr[i];

    mins = arrSum(arr, n);
    maxs = arrSum(arr, n);

    int temparr[n];
    for(int i = 2; i <= d; i++){
        int daysum = 0;

            for(int d = 0; d < n; d++){//refresh array
                temparr[d] = arr[d];
            }

        for(int j = 0; j < n; j++){

            //sides
            if(j == 0){ if(i % 2) arr[j] = 0; else arr[j] = 1; daysum += arr[j]; }
            else if(j == n - 1){ if(i % 2) arr[j] = 0; else arr[j] = 1; daysum += arr[j]; }
            //

            else { //main bodycheck
                int next = temparr[j + 1];
                int prev = temparr[j - 1];
                int state = shine(prev, next, i);
                arr[j] = state;
                daysum += state;
            }
            //
        }
        if(daysum < mins){ mins = daysum; minindex = i; }
        if(daysum > maxs){ maxs = daysum; maxindex = i; }
    }

    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";

    cout << endl << "Naktis, kai sviete daugiausia: " << maxindex << endl;
    cout << "Naktis, kai sviete maziausiai: " << minindex << endl;


    fi.close();

 return 0; }
